import pandas as pd
import geopandas as gpd
import pandas as pd
from shapely.geometry import Point, LineString, Polygon, MultiPolygon
from shapely.geometry import shape
import json 
from pyproj import Proj, Transformer, transform
from shapely.geometry import Point

# Hochmoorfläche
path = r"C:\Users\lluis\OneDrive\Documentos\ETH\H4G\git\wwf\data\geodata\Hochmoor_LV03\hochmoor.shp" #path to shp file
gdf = gpd.read_file(path)
gdf = gdf.to_crs('epsg:4326', epsg = 'epsg:21781') #Going from Swiss to Longitude Latitude coords
#Forests
path2 = r"C:\Users\lluis\OneDrive\Documentos\ETH\H4G\git\wwf\data\forest_geodata\find.json" #path to json file
f = open(path2)
data = json.load(f)
data = data['results']
gdf2 = gpd.GeoDataFrame()
gdf2["geometry"] = [MultiPolygon([Polygon(poly) for poly in data[k]['geometry']['rings']]) for k in range(len(data))]
gdf2["objnummer"] = [data[k]['attributes']['objnummer'] for k in range(len(data))]
gdf2 = gdf2.set_crs('EPSG:21781')
gdf2 = gdf2.to_crs('epsg:4326', epsg = 'epsg:21781') #Going from Swiss to Longitude Latitude coords

def getdatasets():
    '''
    Return datasets from hochmoorflache and forests
    '''
    return gdf, gdf2, data
def changeofcoordinates(point):
    pWorld = Proj(init='epsg:4326')
    pCH = Proj(init='epsg:21781')
    newpoint = transform(pCH,pWorld, point.coords[0][0], point.coords[0][1],always_xy=True) #IN GOOGLE MAPS YOU PUT THE INVERSE ORDER!!!!!!!
    return Point(newpoint[0], newpoint[1])
def changeofcoordinatestuple(tuple):
    pWorld = Proj(init='epsg:4326')
    pCH = Proj(init='epsg:21781')
    newpoint = transform(pCH,pWorld, tuple[0], tuple[1],always_xy=True) #IN GOOGLE MAPS YOU PUT THE INVERSE ORDER!!!!!!!
    return Point(newpoint[0], newpoint[1])
#Google maps format: latitude, longitude

def boundaryreduction(boundary):
    newcoords = []
    i=0
    while i<len(boundary.coords[0:]):
        newcoords.append(boundary.coords[i])
        i+=int(len(boundary.coords[0:])/10)
    newboundary = LineString(newcoords)
    return newboundary

from numpy import infty

def distance(point, boundaries, reducedimensionality=False):
    min = infty
    for boundary in boundaries:
        if reducedimensionality==True:
            boundary = boundaryreduction(boundary)
        dist = boundary.distance(point)
        if min > dist:
            min = dist
        
    return min

def Nclosest(point, gdf, N=1, reducedimensionality=False, typesinrisk = [1,2]):
    '''
    typesinrisk is the types we want to check. By default we want to look for Primäre Hochmoorfläche and Sekundäre Hochmoorfläche
    
    '''
    i = 1
    ordereddistances = [infty]*(N+1)
    keys = [0]*(N+1)
    distancesinkm = [infty]*(N+1)

    while i <= 941:
        dataset = gdf[gdf["ObjNummer"]==str(i)].reset_index(drop = True)
        dataset = dataset[dataset["Typ"].isin([1,2,3,4,5,6])].reset_index(drop=True)
        dista = distance(point, dataset["geometry"].boundary, reducedimensionality = reducedimensionality)
        distainkm = distanceinkm(point, gdf.iloc[i]["geometry"].centroid)


        j = 0
        while j < N:
            if ordereddistances[N-j-1]>dista:
                ordereddistances[N-j] = ordereddistances[N-j-1]
                ordereddistances[N-j-1] = dista
                distancesinkm[N-j] = distancesinkm[N-j-1]
                distancesinkm[N-j-1] = distainkm
                keys[N-j] = keys[N-j-1]
                keys[N-j-1] = i
                j+=1
            else: break
        i+=1
    
    return distancesinkm[:-1], keys[:-1]


def Nclosestforests(point, gdf2, N=1, reducedimensionality = False):
    i = 0
    ordereddistances = [infty]*(N+1)
    distancesinkm = [infty]*(N+1)
    attributes = [None]*(N+1)
    keys = [0]*(N+1)
    while i < gdf2.shape[0]:
        dista = distance(point, gdf2.iloc[i]["geometry"].boundary, reducedimensionality = reducedimensionality)
        distainkm = distanceinkm(point, gdf2.iloc[i]["geometry"].centroid)
        attribute = data[i]['attributes']
        j = 0
        while j < N:
            if ordereddistances[N-j-1]>dista:
                ordereddistances[N-j] = ordereddistances[N-j-1]
                ordereddistances[N-j-1] = dista
                distancesinkm[N-j] = distancesinkm[N-j-1]
                distancesinkm[N-j-1] = distainkm
                attributes[N-j] = attributes[N-j-1]
                attributes[N-j-1] = attribute
                keys[N-j] = keys[N-j-1]
                keys[N-j-1] = i
                j+=1
            else: break
        i+=1
    return keys[:-1], distancesinkm[:-1], attributes[:-1]

import math

def distanceinkm(origin, destination):
    """
    Calculate the Haversine distance.

    Parameters
    ----------
    origin : tuple of float
        (lat, long)
    destination : tuple of float
        (lat, long)

    Returns
    -------
    distance_in_km : float

    Examples
    --------
    >>> origin = (48.1372, 11.5756)  # Munich
    >>> destination = (52.5186, 13.4083)  # Berlin
    >>> round(distance(origin, destination), 1)
    504.2
    """
    origin = origin.coords[0]
    destination = destination.coords[0]
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d

