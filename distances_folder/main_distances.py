import pandas as pd
from distances_folder import distances
import shapely
from shapely.geometry import Point

file = pd.read_excel('prueba.xlsx')

gdf, gdf2, dat= distances.getdatasets()

file['Closest forest'] = 0
file['Closest forest distance'] = 0
file['Closest Hochmoorflache'] = 0
file['Closest Hochmoorflache distance'] = 0

def getpoint(row):
    return Point(row['Longitude'],row['Latitude'])

import warnings
warnings.filterwarnings("ignore")
for i, row in file.iterrows():
    #print(row['meta_id'])
    point = getpoint(row)
    #print('Closest hochmoorfläche:')
    distance, idx  = distances.Nclosest(point, gdf)
    #print(f"The closest Hochmoorflache is {list(gdf['RefObjBlat'].iloc[idx])[0]} and it is {distance[0]} km away")
    #print('Closest forest')
    distanceforest, idxforest, attribute = distances.Nclosestforests(point, gdf2)
    file['Closest Hochmoorflache'].iloc[i] = list(gdf['RefObjBlat'].iloc[idx])[0]
    file['Closest Hochmoorflache distance'].iloc[i] = distance[0]
    file['Closest forest'].iloc[i] = list(attribute)
    file['Closest forest distance'].iloc[i] = distanceforest[0]
    #print(f"The closest Hochmoorflache is {list(attribute)} and it is {distanceforest[0]} km away")

file.to_excel("output.xlsx")  