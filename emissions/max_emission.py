import pandas as pd
import numpy as np

def max_emission(dist: float, environment: str, cst_values: np.array):
    # See the documentation "Stickstoffbelastungen durch Ammoniak- Emissionen von Ställen" for more information on how exactly
    # it is computed and to what corresponds the different profiles (we followed the name given in the paper)
    # Output:
    # - If environment is "Forest" or "Hochmoor", an array of size (2,) containing the maximum emission possible to not go over the
    #   critical level given in the cst_values for NH3 concentration and deposition.
    #   The max emission is taken to be the minimum over the different profiles of the maximum emission allowed for the specific 
    #   profile and environment. Moreover, the max emission is the minimum of 2 different environments corresponding to either "forest"
    #   or "moor" environments.
    # - In any other cases: a pd.DataFrame containing the max emission with respect to NH3 concentration and deposition for all
    #   types of environments (Hochmoor, Flachmoor, Trockenwiese, Laubwald, Nadelwald). The max emission is taken to be the minimum over
    #   the different profiles of the maximum emission allowed for the specific profile and environment.
    # Inputs:
    # - dist represents the distance of the forest with respect to which the maximum emission will be computed,
    #   it has to be greater or equal to 50 (otherwise it will be put automatically to 50).
    # - environment represents the environment with respect to which the maximum emission will be computed. It can
    #   be "Forest", "Hochmoor" or any other strings. If it is set to any other string, then the maximum emission with respect
    #   to each environments will be computed and the output of max_emission will be different.
    # - cst_values is an array containing all the necessary constants for the computation (see how we read the excel sheet
    #   for more details about how the constants are put in this array, if any change is needed)

    if dist < 50:
        dist = 50

    # extracting smallest lower distance index (because the model only allows for some fixed distances)
    scale = cst_values[:,0]
    low_dist_p = scale[scale <= dist][-1]
    index_dist = np.where(scale==low_dist_p)[0][0]

    # global constants needed for computations
    K1, K2 = cst_values[0:2,9]

    # split the cases depending on the environment variable
    if environment=="Forest":
        index_env = range(7,9)
    elif environment=="Hochmoor":
        index_env = range(4,6)
    else:
        E_max_global = []

        # Loop on the type of environment
        for i in range(4,9):

            E_max_profiles = np.zeros((3,2))
            C_crit_i_conc = cst_values[2,i]
            C_crit_i_dep = cst_values[1,i]
            C_Vdep = cst_values[0,i]

            # Loop on the profiles
            for j in range(1,4):
                C_dist_j = cst_values[index_dist,j]

                # NH3-concentration
                E_max_profiles[j-1,0] = C_crit_i_conc / (K1*C_dist_j)

                # NH3-deposition
                E_max_profiles[j-1,1] = C_crit_i_dep / (K2*C_Vdep*K1*C_dist_j)

            E_max_min_profile = np.amin(E_max_profiles, axis=0)
            E_max_min_profile = E_max_min_profile.tolist()
            E_max_global.append(E_max_min_profile)

        E_max_df = pd.DataFrame(E_max_global, index=["Hochmoor", "Flachmoor", "Trockenwiese", "Laubwald", "Nadelwald"], columns=["Max emission wrt NH3 concentration", "Max emission wrt NH3 deposition"])

        return E_max_df

    # Initialise array to store different environments
    E_max_env = np.zeros((2,2))

    for i in index_env:
        C_crit_i_conc = cst_values[2,i]
        C_crit_i_dep = cst_values[1,i]
        C_Vdep = cst_values[0,i]

        # Initialise array to store different profiles
        E_max_profiles = np.zeros((3,2))

        #Loop on the profiles
        for j in range(1,4):
            C_dist_j = cst_values[index_dist,j]

            # NH3-concentration
            E_max_profiles[j-1,0] = C_crit_i_conc / (K1*C_dist_j)

            # NH3-deposition
            E_max_profiles[j-1,1] = C_crit_i_dep / (K2*C_Vdep*K1*C_dist_j)

        # Take the minimum of the profiles
        E_max_min_profile = np.amin(E_max_profiles, axis=0)
        E_max_env[i%2,:] = E_max_min_profile[:]

    # take the minimum of the emission between the environment of interest (forests if environment is "Forest"
    # and moor if environment is "Hochmoor")
    E_max_min_env = np.amin(E_max_env, axis=0)

    return E_max_min_env

def max_emission_add_df(dist: pd.DataFrame, df: pd.DataFrame, cst_values: np.array):
    # Adds 4 columns to the DataFrame df and outputs it.
    # The 4 columns added are 'Max emission wrt closest forest, NH3 concentration',
    # 'Max emission wrt closest forest, NH3 deposition' 'Max emission wrt closest Hochmoor, NH3 concentration' and
    # 'Max emission wrt closest Hochmoor, NH3 deposition'.
    # It uses the max_emission function to add the maximum emission allowed to not exceed the critical level for
    # the closest forest and the closest Hochmoor.

    dist_arr = dist.to_numpy()

    # Store different values computed to avoid to recompute them
    E_max = np.empty((17,2,2))
    E_max.fill(np.nan)

    df['Max emission wrt closest forest, NH3 concentration'] = 0
    df['Max emission wrt closest forest, NH3 deposition'] = 0
    df['Max emission wrt closest Hochmoor, NH3 concentration'] = 0
    df['Max emission wrt closest Hochmoor, NH3 deposition'] = 0

    # Loop on the rows of df
    count = 0
    for k, row in df.iterrows():

        dist_forest = dist_arr[count,0]
        dist_hochmoor = dist_arr[count,1]

        if dist_forest < 50:
            dist_forest = 50
        if dist_hochmoor < 50:
            dist_hochmoor = 50

        scale = cst_values[:,0]
        low_dist_p_f = scale[scale <= dist_forest][-1]
        index_dist_f = np.where(scale==low_dist_p_f)[0][0]

        low_dist_p_h = scale[scale <= dist_hochmoor][-1]
        index_dist_h = np.where(scale==low_dist_p_h)[0][0]

        if np.isnan(E_max[index_dist_f,0,0]):
            E_max_forest = max_emission(dist=dist_forest, environment="Forest", cst_values=cst_values)
            E_max[index_dist_f,:,0] = E_max_forest[:]
        else:
            E_max_forest = E_max[index_dist_f,:,0]

        if np.isnan(E_max[index_dist_h,0,1]):
            E_max_hochmoor = max_emission(dist=dist_hochmoor, environment="Hochmoor", cst_values=cst_values)
            E_max[index_dist_h,:,1] = E_max_hochmoor[:]
        else:
            E_max_hochmoor = E_max[index_dist_h,:,1]

        df['Max emission wrt closest forest, NH3 concentration'].iloc[count] = E_max_forest[0]
        df['Max emission wrt closest forest, NH3 deposition'].iloc[count] = E_max_forest[1]
        df['Max emission wrt closest Hochmoor, NH3 concentration'].iloc[count] = E_max_hochmoor[0]
        df['Max emission wrt closest Hochmoor, NH3 deposition'].iloc[count] = E_max_hochmoor[1]
        count += 1

    return df

