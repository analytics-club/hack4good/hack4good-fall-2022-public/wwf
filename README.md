# WWF



## Briefly about us

Hi there! We are the Nitrogen Pandas: Agustina, Lluís, Philipp and Lilian! Representing WWF in the Hack4Good 2022 (https://analytics-club.org/hack4good/). To see more information of the challenge, please see the documents in "Docs". There you will find the initial documents presenting the initial challenge and the final report explaining our work.

We did this challenge mainly to help a non profit organization as WWF, but in the end we gained an amazing experience where we developed our skills and worked as a team. We hope our work will help WWF workers to accelerate their tasks and invest their time to more amazing contributions.

## Explanation of the different folders

The main program is gui.py, an interface that the stakeholders will use to benefit from our contribution and investigation. In the docs folder, you can find the information of the challenge and the final report. The other folders such as data, web_scrapping, distance_folder and emissions contain both datasets and modules that the gui is based on.

We hope you enjoy our work as much as we did!

