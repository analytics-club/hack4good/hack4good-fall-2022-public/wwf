import os
import tkinter
import customtkinter
from tkcalendar import DateEntry
from tkinter import StringVar, IntVar
from tkinter import Listbox, MULTIPLE, filedialog, E
from customtkinter import CTkProgressBar, CTkButton, CTkLabel, CTkEntry, CTkFrame, CTkCheckBox
from datetime import datetime
import threading
import warnings
warnings.filterwarnings("ignore")

customtkinter.set_appearance_mode("dark")  # Modes: "System" (standard), "Dark", "Light"
customtkinter.set_default_color_theme("dark-blue")  # Themes: "blue" (standard), "green", "dark-blue"

from webscrapping.scrapers import *
from distances_folder import distances
from shapely.geometry import Point
from emissions.max_emission import *


class App(customtkinter.CTk):

    WIDTH = 780
    HEIGHT = 520

    def __init__(self):
        super().__init__()

        self.title("NitrogenPandas")  # TITLE OF THE APPLICATION
        self.geometry(f"{App.WIDTH}x{App.HEIGHT}")
        self.protocol("WM_DELETE_WINDOW", self.on_closing)  # call .on_closing() when app gets closed

        self.resizable(0., 0.)  # MAKES THE WINDOW OF THE APP NON-EXPANDABLE
        self.frame = CTkFrame(master=self)
        self.frame.pack(pady=20, padx=60, fill="both", expand=True)

        self.label = CTkLabel(master=self.frame,
                              justify=tkinter.CENTER,
                              text="SEARCH CRITERIA",
                              text_font="Arial 14 bold")
        self.label.grid(row=0, column=0, padx=20, columnspan=4)

        ##### STARTING AND END DATES INPUTS
        self.label_start = CTkLabel(master=self.frame,
                                    justify=tkinter.LEFT,
                                    text="Please select a start date:")
        self.label_start.grid(row=1, column=0, pady=15, padx=15)

        self.sel_start = StringVar()
        current = datetime.datetime.now()
        self.start_de = DateEntry(self.frame, width=12,
                                  foreground='white', borderwidth=2,
                                  background='black',
                                  headersbackground='white',
                                  headersforground='white',
                                  weekendbackground='white',
                                  weekendforground='black',
                                  selectmode='day',
                                  year=current.year, month=current.month, day=1,
                                  date_pattern="dd/mm/yy",
                                  textvariable=self.sel_start)
        self.start_de.grid(row=1, column=1, pady=15, padx=15)

        self.label_end = CTkLabel(master=self.frame,
                                  justify=tkinter.LEFT,
                                  text="Please select an end date:")
        self.label_end.grid(row=1, column=2, pady=15, padx=15)
        self.sel_end = StringVar()
        self.end_de = DateEntry(self.frame, width=12,
                                foreground='white', borderwidth=2,
                                background='black',
                                headersbackground='white',
                                headersforground='white',
                                weekendbackground='white',
                                weekendforground='black',
                                selectmode='day',
                                year=current.year, month=current.month, day=current.day,
                                date_pattern='dd/mm/yy',
                                textvariable=self.sel_end)
        self.end_de.grid(row=1, column=3, pady=15, padx=15)


        ###### KEYWORDS SELECTION

        kw_var = StringVar(value="You may choose none, one \n"
                                 "or many keywords \n"
                                 " (default searches for all):")
        self.label_kw = CTkLabel(master=self.frame,
                                 textvariable=kw_var,
                                 width=120,
                                 height=25,
                                 corner_radius=8)
        self.label_kw.grid(row=10, column=0, padx=5, pady=15)
        self.keywords = StringVar()
        self.keywords.set("all none Stall Bauernhof Écurie Ferme Stabile Fattoria") ### OPTIONAL LIST FOR KEYWORDS (default will search for all)
        self.listbox = Listbox(self.frame, listvariable=self.keywords,
                               selectmode=MULTIPLE, width=15, height=6,
                               exportselection=0)
        self.listbox.grid(row=10, column=1, padx=0, pady=15)

        kw_var2 = StringVar(value="Would you like to add \n any keywords?\n"
                                  "If several, please introduce \n"
                                  " the words separated \n by a comma.")

        self.label_kw2 = CTkLabel(master=self.frame,
                                  textvariable=kw_var2,
                                  width=120,
                                  height=25,
                                  corner_radius=8)
        self.label_kw2.grid(row=10, column=2, padx=5, pady=15)
        self.kw2 = CTkEntry(self.frame, width=120)   ##### ENTRY FOR MORE KEYWORDS
        self.kw2.grid(row=10, column=3, padx=5, pady=15)


        ##### CANTON(s) SELECTION

        ca_var = StringVar(value="You may choose one\n"
                                 " or more cantons \n"
                                 "(default searches for all).")

        self.label_ca = CTkLabel(master=self.frame,
                                 textvariable=ca_var,
                                 width=120,
                                 height=25,
                                 corner_radius=8)
        self.label_ca.grid(row=20, column=0, padx=5, pady=15)

        self.keywords_ca = StringVar()
        self.keywords_ca.set("all ZH LU BE AR BS")  ### OPTIONAL LIST (default will search for this 5)
        self.listbox_ca = Listbox(self.frame, listvariable=self.keywords_ca,
                                  selectmode=MULTIPLE, width=15, height=6,
                                  exportselection=0)
        self.listbox_ca.grid(row=20, column=1, padx=5, pady=15)

        ##### ONLY AGRICULTURAL ZONES CHECK

        self.CheckVar = IntVar(value=1)
        self.check_box = CTkCheckBox(self.frame, text="Only agricultural zones", variable=self.CheckVar)
        self.check_box.grid(row=20, column=2, padx=5, pady=15, columnspan=2)


        ##### IMPORTANT: CODE WON'T START IF A FOLDER IS NOT SPECIFIED

        dir_var = StringVar(value="Please indicate the location \n "
                                  "where the final excel df \n will be saved to.")

        self.label_dir = CTkLabel(master=self.frame,
                                  textvariable=dir_var,
                                  width=120,
                                  height=25,
                                  corner_radius=8)
        self.label_dir.grid(row=30, column=0, padx=5, pady=10, columnspan=1)

        self.chooseDir = CTkButton(self.frame,
                                   text="Select",
                                   width=20, height=3,
                                   command=self.chooseDir)
        self.chooseDir.grid(row=30, column=1, pady=10, padx=5, columnspan=1)
        self.chooseDir.width = 100

        self.dir = StringVar()
        self.dir.set("No folder selected")
        self.label_dir2 = CTkLabel(master=self.frame,
                                   textvariable=self.dir,
                                   width=120, height=25,
                                   corner_radius=8)
        self.label_dir2.grid(row=30, column=2, padx=5, pady=10, columnspan=2, rowspan=5)


        ###### START BUTTON

        self.message = StringVar(value="Start")

        self.start_button = CTkButton(
            self.frame,
            textvariable=self.message, text_font="Arial 12 bold",
            command=threading.Thread(target=self.start_search).start,
            fg_color="gray",
            state="disabled"
        )
        self.start_button.grid(column=0, row=35, padx=5, pady=5, columnspan=2, sticky=E)

    def start_search(self, ):

        #### while searching, the code cannot be stopped
        ### if you want to cancel it, you have to close it and restart it
        self.message.set("Searching")
        self.start_button.configure(state="disabled")

        ### progresss bar
        self.pb = CTkProgressBar(
            self.frame,
            orient='horizontal',
            mode='indeterminate',
        )
        # place the progressbar and start it
        self.pb.grid(column=2, row=35, columnspan=2, padx=5, pady=5)
        self.pb.start()

        #### scrape the information from the user interface
        start_date = self.sel_start.get()
        start_day, start_month, start_year = start_date.split("/")
        end_date = self.sel_end.get()
        end_day, end_month, end_year = end_date.split("/")

        keywords = []
        for i in self.listbox.curselection():
            keywords.append(self.listbox.get(i))

        if keywords == [] or keywords[0] == "all":
            keywords = ["Stall", "Bauernhof", "Écurie", "Ferme", "Stabile", "Fattoria"]
        elif keywords[0] == "none":
            keywords = []
        more_keywords = self.kw2.get()
        if more_keywords != "":
            more_keywords = more_keywords.split(",")
            for element in more_keywords:
                if element != "":
                    keywords.append(element)


        cantons = []
        for i in self.listbox_ca.curselection():
            cantons.append(self.listbox_ca.get(i))
        if cantons == [] or cantons[0] == "all":
            cantons = ["ZH", "LU", "BE", "AR" ,"BS"]

        if self.check_box.get():
            only_agricultural = True
        else:
            only_agricultural = False

        ####### WEB SCRAPING PART
        #### These keys are the data from the permits that we are interested in
        keys_interest = ["meta_id",
                         "meta_registrationOffice_id",
                         "meta_registrationOffice_displayName",
                         "meta_publicationNumber",
                         "meta_publicationDate",
                         "meta_expirationDate",
                         "meta_cantons",
                         "content_buildingContractor_legalEntity_multi_companies_company_name",
                         "content_projectDescription",
                         "content_projectLocation_address_street",
                         "content_projectLocation_address_houseNumber",
                         "content_projectLocation_address_swissZipCode",
                         "content_projectLocation_address_town",
                         "content_districtCadastre_relation_cadastre",
                         "content_districtCadastre_relation_buildingZone"]
        df_main = pd.DataFrame(columns=keys_interest)

        if "all" in cantons:
            df_main = scrape_API(df_main,
                cantons=cantons,
                kw=keywords,
                s_d=start_day,
                s_m=start_month,
                s_y=start_year,
                e_d=start_day,
                e_m=start_month,
                e_y=start_year,
                only=only_agricultural,
            )

            df_main = scrape_LU(df_main,
                                kws=keywords,
                                s_d=int(start_day),
                                s_m=int(start_month),
                                s_y=int(start_year),
                                e_d=int(end_day),
                                e_m=int(end_month),
                                e_y=int(end_year),
                                only=only_agricultural,
                                )

        else:
            if "ZH" in cantons or "TI" in cantons or "BE" in cantons \
                    or "AR" in cantons or "BS" in cantons:
                df_main = scrape_API(df_main,
                    cantons=cantons,
                    kws=keywords,
                    s_d=start_day,
                    s_m=start_month,
                    s_y=start_year,
                    e_d=end_day,
                    e_m=end_month,
                    e_y=end_year,
                    only=only_agricultural,
                )


            if "LU" in cantons:
                df_main = scrape_LU(df_main,
                    kws=keywords,
                    s_d=int(start_day),
                    s_m=int(start_month),
                    s_y=int(start_year),
                    e_d=int(end_day),
                    e_m=int(end_month),
                    e_y=int(end_year),
                    only=only_agricultural,
                )
            # if "SG" in cantons:
            #     df_3 = filter_SG()
            # if "OW" in cantons:
            #     df_4 = filter_OW()
            #
            #

        df_main = df_main[df_main.Latitude != "Keine Angaben"]
        df_main = df_main[df_main.Longitude != "Keine Angaben"]
        df_main = df_main.loc[:, ~df_main.columns.str.match("Unnamed")]

        # df_main["Latitude"] = df_main["Latitude"].astype(float)
        # df_main["Longitude"] = df_main["Longitude"].astype(float)
        # df_main.to_excel("C:\\Users\\agus0\\Downloads\\prueba.xlsx")
        ####### DISTANCE TO SENSITIVE FOREST OR HOCHMOORFLACE CALCULATION
        # FROM THE LATITUTE AND LONGITUDE DATA OF EACH PROJECT

        gdf, gdf2, dat = distances.getdatasets()

        df_main = df_main.loc[:, ~df_main.columns.str.contains('^Unnamed')]
        df_main['Closest_forest'] = 0
        df_main['Closest_forest_distance'] = 0
        df_main['Closest_Hochmoorflache'] = 0
        df_main['Closest_Hochmoorflache_distance'] = 0

        def getpoint(row):
            return Point(row['Longitude'], row['Latitude'])

        count = 0
        for i, row in df_main.iterrows():
            point = getpoint(row)
            distance, idx = distances.Nclosest(point, gdf)
            distanceforest, idxforest, attribute = distances.Nclosestforests(point, gdf2)
            dict_info = list(attribute)[0]
            # print(dict_info["objnummer"])
            df_main['Closest_Hochmoorflache'].iloc[count] = list(gdf['RefObjBlat'].iloc[idx])[0]
            df_main['Closest_Hochmoorflache_distance'].iloc[count] = distance[0]
            df_main['Closest_forest'].iloc[count] = dict_info['objnummer']
            df_main['Closest_forest_distance'].iloc[count] = distanceforest[0]
            count += 1


        ### EMISSIONS ESTIMATION

        # extracting data from the excel sheet in a DataFrame
        file_path = "emissions/model.xlsx"
        df_xlsx = pd.read_excel(file_path)

        cst_values = np.zeros((17, 10))

        # adding constants for the model
        cst_values[0:17, 0:4] = df_xlsx.iloc[5:22,0:4]

        # adding Hochmoor, Flachmoor, Trockenwiese, Laubwald and Nadelwald constants
        cst_values[0:3,4:9] = df_xlsx.iloc[3:6,6:11]

        # supplementary constants
        cst_values[0:2,9] = df_xlsx.iloc[25:27,1]

        df_main = max_emission_add_df(df_main[['Closest_forest_distance',
                                               'Closest_Hochmoorflache_distance']],
                                      df_main, cst_values)

        ### FLAGGING OF PROJECTS


        df_main["Max_emission_allowed"] = df_main[['Max emission wrt closest Hochmoor, NH3 concentration',
                                                   'Max emission wrt closest forest, NH3 concentration']].min(axis=1)


        df_main = df_main.sort_values(by=['Max_emission_allowed'],ascending=False)
        path_excel = self.path + "\output.xlsx"
        print(path_excel)
        if os.path.exists(path_excel):  # check if previous file exists

            df_previous = pd.read_excel(path_excel)
            df_main = pd.concat([df_previous, df_main], axis=0)  # concatenate files
            df_main = df_main.drop_duplicates()  # remove duplicates
        df_main.to_excel(path_excel)
        self.pb.stop()
        self.message.set("DONE")
        self.pb.stop()

    def chooseDir(self, ):
        folder = filedialog.askdirectory(parent=self.frame,
                                       initialdir="/",
                                       title='Please select a directory')

        if len(folder) > 50:
            csv_folder_split = folder.split("/")
            text = ""
            line = ""
            for i in range(len(csv_folder_split)):
                if len(line + csv_folder_split[i]) < 50:
                    line += csv_folder_split[i] + "/"
                    if i == (len(csv_folder_split) - 1):
                        text += line

                else:
                    line += "\n"
                    text += line
                    line = ""
                    line += csv_folder_split[i] + "/"
        else:
            text = folder
        # print(text)
        self.path = text
        self.dir.set(text)
        self.start_button.configure(state="enable", text_color="white",
                                    fg_color=['#608BD5', '#395E9C'])

    def on_closing(self, event=0):
        self.destroy()



if __name__ == "__main__":
    app = App()
    app.mainloop()