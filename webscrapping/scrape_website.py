
import requests
import validators
import sys
from bs4 import BeautifulSoup as bs
from urllib.parse import urlparse
import wget
from urllib.request import urlopen, Request
import urllib.request
import PyPDF2 as pdf2
from io import BytesIO
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

nltk.download('stopwords')
nltk.download('punkt')
punctuations=["(", ")", ";", ":", "[", "]", ",", ".", "..."]
def check_validity(my_url):
    try:
        urlopen(my_url)
    except IOError:
        sys.exit()
    # urlopen(my_url)

def get_pdfs(my_url):
    links = []

    html = urlopen(my_url).read()
    html_page = bs(html, features="lxml")
    og_url = html_page.find("meta",  property = "og:url")
    base = urlparse(my_url)

    # print(html_page.find_all('a'))
    for link in html_page.find_all('a'):
        # print(link)
        current_link = link.get('href')
        if current_link.startswith('/ekab'):
            links.append(base.scheme + "://" + base.netloc + current_link)


    print(links)

    for i, link in enumerate(links):
        if i > 0:
            continue
        link = link.replace("/publikation/", "/pdf/")

        wFile = urllib.request.urlopen(link)
    #     wFile = urllib.request.urlopen(link)
        pdfreader = pdf2.PdfFileReader(BytesIO(wFile.read()))
        num_pages = pdfreader.numPages
        filtered_words =[]
        text = []
    #     for i in range(num_pages):
    #         pageObj = pdfreader.getPage(i)
    #         page = pageObj.extractText()
    #         text_page = pageObj.extractText().splitlines()
    #         for line in text_page:
    #             print(line)
    #         # print(text_page)
    #         text.append(text_page)
    #         tokens = word_tokenize(page)
    #         stop_words = stopwords.words('german')
    #         # print(list(stopwords.words('english')))
    #         filtered_words_page = [word for word in tokens if word not in stopwords.words('english') and not word in punctuations]
    #         filtered_words.append(filtered_words_page)
    #         # print(filtered_words)
    # #     #     except:
    # #         print(" \n \n Unable to Download A File \n")
    print('\n')


def main():
    print("Enter Link: ")
    my_url = "https://publikationen.sg.ch/amtliche-publikationen/?searchQuery=stall&filter%5Bcategory%5D%5B0%5D=172%2C180&filter%5Btype%5D%5B0%5D=tx_ekab_publication_domain_model_publication&timerange%5Btype%5D=4"
    # check_validity(my_url)
    get_pdfs(my_url)

main()