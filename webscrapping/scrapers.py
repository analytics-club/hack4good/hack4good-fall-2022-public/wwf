import collections
try:
    collectionsAbc = collections.abc
except AttributeError:
    collectionsAbc = collections
import xmltodict
import requests
from PyPDF2 import PdfReader
from io import BytesIO
import datetime
import pandas as pd
from geopy.geocoders import Nominatim
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collectionsAbc.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def scrape_API(df_main,
               cantons="all",
               kws=[],
               s_d=1,
               s_m=1,
               s_y=2022,
               e_d=1,
               e_m=1,
               e_y=2022,
               only=True):

    """**Set parameters**

    *possible parameters defined by API: https://amtsblattportal.ch/docs/api/*

    *   Date:
    <br> filter for publications within a a *start* and *end* date
    *   Keyword:
    <br> filter publications by single keyword
    *   Rubric:
    <br> filter by rubric. In this case 'BP' will be used to filter for 'Bauprojekt' followed by '-' and the short code of the desired canton

    """
    # api - Base URL
    api_url = "https://amtsblattportal.ch/api/v1/publications/xml?"

    # date
    # Year-Month-Day
    start_date = str(s_y) + "-" + str(s_m) + "-" + str(s_d)
    end_date = str(e_y) + "-" + str(e_m) + "-" + str(e_d)

    # keyword
    # Only whole words are matched. However one can also use a wildcard sign "*" for enhanced search
    # (i.e. "keyword=Bank*" parameter will also find publications containing the word "Bankruptcy").
    # rubrics
    # Bauproject = 'BP'
    # Add shortcode for canton
    # options for Cantons are [ZH, BE, AR, BS, TI]

    # formatting
    if kws != []:
        kws = ["&keyword=" + kw for kw in kws]

    rubrics = ["&rubrics=" + 'BP-' + ca for ca in cantons]
    PARAMS = "publicationStates=PUBLISHED" + "&publicationDate.start=" + start_date + "&publicationDate.end=" + end_date

    """Use "PARAMS=PARAMS+date+rubric+keyword" to filter with all 3 options. 
    <br> Otherwise leave options out, like "PARAMS=PARAMS+date+rubric" which only filters for rubric and date but leaves out keywords 
    """
    XML_list = []
    for ru in rubrics:
        if kws != []:
            for kw in kws:
                # print(api_url + PARAMS + ru + kw)
                response = requests.get(url=api_url + PARAMS + ru + kw)
                """The response XML file does NOT contain the content of a publication. To receive the entire publication, the resource given in the ref element has to be requested."""

                temp = xmltodict.parse(response.text)
                bulk_dict = {}
                try:
                    bulk_dict = temp['bulk:bulk-export']['publication']
                except KeyError:
                    print('No publications found with given search criteria')
                    continue
                except:
                    print('Problem with search criteria')
                    continue

                if isinstance(bulk_dict, dict):
                    XML_list.append("https://amtsblattportal.ch/api/v1/publications/" + bulk_dict['meta']['id'] + "/xml")
                    # print("https://amtsblattportal.ch/api/v1/publications/" + bulk_dict['meta']['id'] + "/xml")
                elif isinstance(bulk_dict, list):
                    length = len(bulk_dict)
                    for i in range(length):
                        XML_list.append("https://amtsblattportal.ch/api/v1/publications/" + bulk_dict[i]['meta']['id'] + "/xml")
        else:
            # print(api_url + PARAMS + ru)
            response = requests.get(url=api_url + PARAMS + ru)

            """The response XML file does NOT contain the content of a publication. To receive the entire publication, the resource given in the ref element has to be requested."""

            temp = xmltodict.parse(response.text)
            bulk_dict = {}
            try:
                bulk_dict = temp['bulk:bulk-export']['publication']
            except KeyError:
                print('No publications found with given search criteria')
                continue
            except:
                print('Problem with search criteria')
                continue

            if isinstance(bulk_dict, dict):
                XML_list.append("https://amtsblattportal.ch/api/v1/publications/" + bulk_dict['meta']['id'] + "/xml")
                print("https://amtsblattportal.ch/api/v1/publications/" + bulk_dict['meta']['id'] + "/xml")
            elif isinstance(bulk_dict, list):
                length = len(bulk_dict)
                for i in range(length):
                    XML_list.append(
                        "https://amtsblattportal.ch/api/v1/publications/" + bulk_dict[i]['meta']['id'] + "/xml")
    # URL_list = []
    # for i in range(length):
    #     URL_list.append('https://www.amtsblatt.zh.ch/#!/search/publications/detail/' + ref_list[i])

    """XML_list contains amtsblätter in XML format
    <br> URL_list contains URL of amtsblätter
    """



    for xml in XML_list:
        # print(xml)
        response = requests.get(url=xml)
        response.encoding = "utf-8"
        my_xml = response.text
        xmldict = xmltodict.parse(my_xml)


        output = flatten(xmldict)
        output2 = {}
        for k, v in output.items():
            if ":publication_" in k:
                _, new_k = k.split(":publication_")
                if new_k in df_main.columns:
                    output2[new_k] = v

        df = pd.DataFrame.from_dict(output2, orient='index')
        df = df.T


        key_address = ["content_projectLocation_address_street",
                         "content_projectLocation_address_houseNumber",
                         "content_projectLocation_address_swissZipCode",
                         "content_projectLocation_address_town"]
        address = ""
        for key in key_address:
            try:
                if "houseNumber" in key:
                    houseNumber = output2[key]
                    houseNumber2 = ""
                    if not houseNumber.isnumeric():
                        for element in list(houseNumber):
                            if element.isnumeric():
                                houseNumber2 += element
                    output2[key] = houseNumber2
                address += output2[key] + ","
            except:
                pass

        geolocator = Nominatim(user_agent='my request')

        try:
            location = geolocator.geocode(address)
            df["Latitude"] = location.latitude
            df["Longitude"] = location.longitude
            df_main = pd.concat([df_main, df], axis=0)

        except:
            try:
                key_address = ["content_projectLocation_address_street",
                               "content_projectLocation_address_swissZipCode"]
                address2 = ""
                for key in key_address:
                    try:
                        address2 += output2[key] + ","
                    except:
                        pass

                location = geolocator.geocode(address2)

                df["Latitude"] = location.latitude
                df["Longitude"] = location.longitude

                df_main = pd.concat([df_main, df], axis=0)

            except:
                # print(address2)
                continue

    return df_main



# helper functions
def int2roman(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "X", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant1(number):
    numerals = {1: "\nI", 4: "IV", 5: "V", 9: "IX", 10: "X", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant2(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "X\n", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant3(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "X ", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant4(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "\nX", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant5(number):
    numerals = {1: "I", 4: "IV", 5: "V\n", 9: "IX", 10: "X", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant6(number):
    numerals = {1: "I", 4: "IV", 5: "\nV", 9: "IX", 10: "X", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant7(number):
    numerals = {1: "I", 4: "I\nV", 5: "V", 9: "IX", 10: "X", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant8(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "X", 40: "X L",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant9(number):
    numerals = {1: "I", 4: " IV", 5: "V", 9: "IX", 10: "X", 40: "X L",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

def int2roman_variant10(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "X ", 40: "X L",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result

# helper function to extract the text in "Öffentliche Planauflagen" from pdf
def get_text(PDF_IN):
    outlines = PDF_IN.getOutlines()
    for idx, item in enumerate(outlines):
        if (not type(item) == list):
            if (item.title == 'Allgemeiner Teil'):
                outlines = outlines[idx + 1]
    for idx, item in enumerate(outlines):
        if (not type(item) == list):
            if (item.title == 'Planungs- und Baurecht'):
                last_page = PDF_IN.getDestinationPageNumber(outlines[idx + 2])
                outlines = outlines[idx + 1]
    for idx, item in enumerate(outlines):
        if (item.title == 'Öffentliche Planauflagen'):
            first_page = PDF_IN.getDestinationPageNumber(item)
            try:
                last_page = PDF_IN.getDestinationPageNumber(outlines[idx + 1])
            except:
                None

    text = ''
    for i in range(first_page, last_page + 1):
        text = text + PDF_IN.pages[i].extractText()
    return text

# helper function to distinguish between building projects in "Öffentliche Planauflagen"
# returns: list of of strings with the building projects

###########################variants###########################
# variants must be added because pdf reader extract text from pdf in different ways
# roman numeral "XLIV" could be "\nX L IV.\n" or "XL\nIV.\n"
# a missing variant here will result in the last item in the list to include all the text after the last building project
##############################################################

def get_planauflagen(pdf_URL):
    response = requests.get(pdf_URL, verify=False, headers={'User-agent': 'wget/1.21.3'})
    try:
        response.raise_for_status()
    except:
        return []
    text = get_text(PdfReader(BytesIO(response.content)))
    nr = 1
    delimiters = []
    list_planauflagen = []
    while (1):
        variants = [('\n' + int2roman(nr) + '.\n'), ('\n' + int2roman(nr) + '\n.\n'),
                    (int2roman_variant1(nr) + '.\n'), ('\n' + int2roman_variant2(nr) + '.\n'),
                    ('\n' + int2roman_variant3(nr) + '.\n'), (int2roman_variant4(nr) + '.\n'),
                    ('\n' + int2roman_variant5(nr) + '.\n'), (int2roman_variant6(nr) + '.\n'),
                    ('\n' + int2roman_variant7(nr) + '.\n'), ('\n' + int2roman(nr) + ' .\n'),
                    ('\n' + int2roman_variant8(nr) + '.\n'), ('\n' + int2roman_variant9(nr) + '.\n'),
                    ('\n' + int2roman_variant10(nr) + '.\n')]
        next = ''
        for variant in variants:
            if variant in text:
                if next == '':
                    next = variant
                else:
                    if (text.find(variant) < text.find(next)):
                        next = variant
        if next != '':
            if (nr != 1):
                list_planauflagen.append(text[:text.find(next)])
            text = text[text.find(next) + len(next):]
            delimiters.append(next)
            nr = nr + 1
        else:
            try:
                list_planauflagen.append(text[:text.find('Öffentliche Beschaffungen')])
            except:
                list_planauflagen.append(text)
            break
    return list_planauflagen

# helper function to find all building projects which include the keywords
def keyword_filter(list_planauflagen, lst_keywords):
    keyword_filtered_planauflagen = []
    for planauflage in list_planauflagen:
        for keyword in lst_keywords:
            if keyword in planauflage:
                keyword_filtered_planauflagen.append(planauflage)
                break
    return keyword_filtered_planauflagen

# helper function to find all building projects which iare part of "Landwirtschaftszone"
def Landwirtschaft_filter(list_planauflagen):
    Landwirtschaft_filtered_planauflagen = []
    for planauflage in list_planauflagen:
        if 'Zone: Landwirtschaftszone' in planauflage:
            Landwirtschaft_filtered_planauflagen.append(planauflage)
    return Landwirtschaft_filtered_planauflagen

def get_final_info(selected_planauflagen):
    Gemeinde = {}
    Bauvorhaben = {}
    Koordinaten = {}
    Gesuchsteller = {}
    for key, lst in selected_planauflagen.items():
        Gemeinde[key] = []
        Bauvorhaben[key] = []
        Koordinaten[key] = []
        Gesuchsteller[key] = []
        for planauflage in lst:
            # find Gemeinde
            temp = planauflage[planauflage.find('Gemeinde:') + 10:planauflage.find('Gemeinde:') + 10 + planauflage[
                                                                                                       planauflage.find(
                                                                                                           'Gemeinde:') + 10:].find(
                ':')]
            if (temp == 'ern'):
                temp = 'Stadt Luzern'
            Gemeinde[key].append(temp)

            # find Bauvorhaben
            temp = ''
            if ('Bauvorhaben:' in planauflage):
                try:
                    # find first ':' after Bauvorhaben
                    test = (planauflage[planauflage.find('Bauvorhaben:') + 13:].find(':'))
                    if (test == -1):
                        # take sentence before first '.' after Bauvorhaben
                        temp = planauflage[planauflage.find('Bauvorhaben:') + 13:planauflage.find(
                            'Bauvorhaben:') + 13 + planauflage[planauflage.find('Bauvorhaben:') + 13:].find('.')]
                    else:
                        # find first ':' after Bauvorhaben and take off word before ':'
                        temp = planauflage[
                               planauflage.find('Bauvorhaben:') + 13:planauflage.find('Bauvorhaben:') + 13 + (
                                   planauflage[planauflage.find('Bauvorhaben:') + 13:].find(':'))]
                        temp = temp[:temp.rfind('.')]
                except:
                    try:
                        # take first sentence after Bauvorhaben
                        temp = planauflage[planauflage.find('Bauvorhaben:') + 13:planauflage.find(
                            'Bauvorhaben:') + 13 + planauflage[planauflage.find('Bauvorhaben:') + 13:].find('.')]
                    except:
                        temp = 'Angaben nicht erreicht'
            elif ('Projekt:' in planauflage):
                try:
                    # find first ':' after Projekt
                    test = (planauflage[planauflage.find('Projekt:') + 8:].find(':'))
                    if (test == -1):
                        # take sentence before first '.' after Projekt
                        temp = planauflage[
                               planauflage.find('Projekt:') + 8:planauflage.find('Projekt:') + 8 + planauflage[
                                                                                                   planauflage.find(
                                                                                                       'Projekt:') + 8:].find(
                                   '.')]
                    else:
                        # find first ':' after Projekt and take off word before ':'
                        temp = planauflage[planauflage.find('Projekt:') + 8:planauflage.find('Projekt:') + 8 + (
                            planauflage[planauflage.find('Projekt:') + 8:].find(':'))]
                        temp = temp[:temp.rfind('.')]
                except:
                    try:
                        # take first sentence after Projekt
                        temp = planauflage[
                               planauflage.find('Projekt:') + 8:planauflage.find('Projekt:') + 8 + planauflage[
                                                                                                   planauflage.find(
                                                                                                       'Projekt:') + 8:].find(
                                   '.')]
                    except:
                        temp = 'Angaben nicht erreicht'
            elif ('Bauprojekt:' in planauflage):
                try:
                    # find first ':' after Bauprojekt
                    test = (planauflage[planauflage.find('Bauprojekt:') + 11:].find(':'))
                    if (test == -1):
                        # take sentence before first '.' after Bauprojekt
                        temp = planauflage[planauflage.find('Bauprojekt:') + 11:planauflage.find(
                            'Bauprojekt:') + 11 + planauflage[planauflage.find('Bauprojekt:') + 11:].find('.')]
                    else:
                        # find first ':' after Bauprojekt and take off word before ':'
                        temp = planauflage[
                               planauflage.find('Bauprojekt:') + 11:planauflage.find('Bauprojekt:') + 11 + (
                                   planauflage[planauflage.find('Bauprojekt:') + 11:].find(':'))]
                        temp = temp[:temp.rfind('.')]
                except:
                    try:
                        # take first sentence after Bauprojekt
                        temp = planauflage[planauflage.find('Bauprojekt:') + 11:planauflage.find(
                            'Bauprojekt:') + 11 + planauflage[planauflage.find('Bauprojekt:') + 11:].find('.')]
                    except:
                        temp = 'Angaben nicht erreicht'
            else:
                temp = 'Angaben nicht erreicht'
            temp = temp.replace('\n', '')
            temp = temp.replace('\xad', '')
            Bauvorhaben[key].append(temp)

            # find Koordinaten
            temp = 'Keine Angaben'
            if ('Koordinaten:' in planauflage):
                text = planauflage.replace('\n', '')
                temp = text[text.find('Koordinaten:') + 13:text.find('Koordinaten:') + 34]
            Koordinaten[key].append(temp)

            # find Gesuchsteller
            temp = ''
            if ('Gesuchsteller:' in planauflage):
                try:
                    # find first ':' after Gesuchsteller
                    temp = planauflage[
                           planauflage.find('Gesuchsteller:') + 15:planauflage.find('Gesuchsteller:') + 15 + (
                               planauflage[planauflage.find('Gesuchsteller:') + 15:].find(':'))]
                    # take off word before ':'
                    temp = temp[:temp.rfind('.')]
                    temp = temp[temp.find(',') + 2:]
                    temp = temp.replace('\n', '')
                    temp = temp.replace('\xad', '')
                except:
                    temp = 'Angaben nicht erreicht'
                Gesuchsteller[key].append(temp)
            elif ('Gesuchstellerin:' in planauflage):
                try:
                    # find first ':' after Gesuchstellerin
                    temp = planauflage[
                           planauflage.find('Gesuchstellerin:') + 17:planauflage.find('Gesuchstellerin:') + 17 + (
                               planauflage[planauflage.find('Gesuchstellerin:') + 17:].find(':'))]
                    # take off word before ':'
                    temp = temp[:temp.rfind('.')]
                    # find first ',' (this is usually the first name)
                    temp = temp[temp.find(',') + 2:]
                    temp = temp.replace('\n', '')
                    temp = temp.replace('\xad', '')
                except:
                    temp = 'Angaben nicht erreicht'
                Gesuchsteller[key].append(temp)
            elif ('Gesuchsteller und Grundeigentümer:' in planauflage):
                try:
                    # find first ':' after Gesuchsteller und Grundeigentümer
                    temp = planauflage[planauflage.find('Gesuchsteller und Grundeigentümer:') + 35:planauflage.find(
                        'Gesuchsteller und Grundeigentümer:') + 35 + (planauflage[planauflage.find(
                        'Gesuchsteller und Grundeigentümer:') + 35:].find(':'))]
                    # take off word before ':'
                    temp = temp[:temp.rfind('.')]
                    # find first ',' (this is usually the first name)
                    temp = temp[temp.find(',') + 2:]
                    temp = temp.replace('\n', '')
                    temp = temp.replace('\xad', '')
                except:
                    temp = 'Angaben nicht erreicht'
                Gesuchsteller[key].append(temp)
            elif ('Grundeigentümer:' in planauflage):
                try:
                    # find first ':' after Grundeigentümer
                    temp = planauflage[
                           planauflage.find('Grundeigentümer:') + 17:planauflage.find('Grundeigentümer:') + 17 + (
                               planauflage[planauflage.find('Grundeigentümer:') + 17:].find(':'))]
                    # take off word before ':'
                    temp = temp[:temp.rfind('.')]
                    # find first ',' (this is usually the first name)
                    temp = temp[temp.find(',') + 2:]
                    temp = temp.replace('\n', '')
                    temp = temp.replace('\xad', '')
                except:
                    temp = 'Angaben nicht erreicht'
                Gesuchsteller[key].append(temp)
            else:
                Gesuchsteller[key].append('Angaben nicht erreicht')
    return Bauvorhaben, Koordinaten, Gesuchsteller

def scrape_LU(df_main_o, kws=['stall'], s_d=1, s_m=1, s_y=2022, e_d=1, e_m=2, e_y=2022, only=True):
    # (year, month, day)
    start_date = datetime.date(year=2000+s_y,month=s_m,day=s_d)
    end_date = datetime.date(year=2000+e_y,month=e_m,day=e_d)

    keywords = kws
    Landwirtschaftszone = only

    # create dictionary with all building projects from Luzerner Kantonsblätter
    # playauflagen_dict={
    # 1: <list of all planauflagen in amtsblatt 1>,
    # 2: <list of all planauflagen in amtsblatt 2>, ... }
    planauflagen_dict = {}

    # get all pdfs between start and end date
    # assumption: start date is after the year 2010
    standard_URL = 'https://www.luzernerkantonsblatt.ch/Kantonsblatt/Archiv/pdf_'
    if ((start_date.isocalendar()[1] > 5 and s_m == 1)):  # first week might be in the previous year
        start_date = start_date + datetime.timedelta(weeks=1)
    total = 0
    while ((start_date.year == end_date.year and start_date.isocalendar()[1] != (end_date.isocalendar()[1])) or (start_date.year != end_date.year)):
        # create URL
        if (start_date.isocalendar()[1] < 10):

            URL = standard_URL + str(start_date.year) + '/kb-' + str(start_date.year - 2000) + '-0' + str(
                start_date.isocalendar()[1]) + '.pdf'
        else:

            URL = standard_URL + str(start_date.year) + '/kb-' + str(start_date.year - 2000) + '-' + str(
                start_date.isocalendar()[1]) + '.pdf'

        # get planauflagen from URL
        planauflagen_dict[start_date.isocalendar()[1]] = get_planauflagen(URL)
        total += 1
        # add one week to start_date
        start_date = start_date + datetime.timedelta(weeks=1)
        if (
                start_date.isocalendar()[1] == end_date.isocalendar()[1] and start_date.year == end_date.year + 1):  # edge case: last week might be in the next year
            break
    print('Total number of amtsblätter searched: ' + str(total))

    # filter for keywords
    keyword_filtered = {}
    for key, lst in planauflagen_dict.items():
        keyword_filtered[key] = keyword_filter(lst, keywords)

    # filter (if necessary) for only Landwirtschaftszone
    if (Landwirtschaftszone == True):
        planauflagen_filtered = {}
        for key, lst in keyword_filtered.items():
            planauflagen_filtered[key] = Landwirtschaft_filter(lst)
    else:
        planauflagen_filtered = keyword_filtered

    Bauvorhaben, Koordinaten, Gesuchsteller = get_final_info(planauflagen_filtered)
    keys_interest = ["meta_id",
                     "content_projectDescription",
                     "content_projectLocation_address",
                     "Latitude",
                     "Longitude"]

    df_main = pd.DataFrame(columns=keys_interest)
    key_list = []
    for key, item in Bauvorhaben.items():
        for planauflage in item:
            key_list.append(key)
    latitude_list = []
    longitude_list = []
    for lst in Koordinaten.values():
        for koordinate in lst:
            latitude_list.append(koordinate if koordinate == 'Keine Angaben' else koordinate[:koordinate.find(' ')])
            longitude_list.append(
                koordinate if koordinate == 'Keine Angaben' else koordinate[koordinate.find('/ ') + 1:])
    Bauvorhaben_list = []
    for lst in Bauvorhaben.values():
        for Bauvorhaben in lst:
            Bauvorhaben_list.append(Bauvorhaben)
    Gesuchsteller_list = []
    for lst in Gesuchsteller.values():
        for Gesuchsteller in lst:
            Gesuchsteller_list.append(Gesuchsteller)

    # get koordinates from geopy
    app = Nominatim(user_agent="Hack4Good")
    for idx, adress in enumerate(Gesuchsteller_list):
        if (latitude_list[idx] == 'Keine Angaben' and adress != 'Angaben nicht erreicht'):
            try:
                location = app.geocode(adress)
                latitude_list[idx] = location.latitude
                longitude_list[idx] = location.longitude
            except:
                pass

    df_main["meta_id"] = key_list
    df_main["meta_cantons"] = ["LU"] * len(key_list)
    df_main["content_projectDescription"] = Bauvorhaben_list
    df_main["content_projectLocation_address"] = Gesuchsteller_list
    df_main["Latitude"] = latitude_list
    df_main["Longitude"] = longitude_list


    return pd.concat([df_main_o, df_main], axis=0)

    # return pd.concat([df_main, df], axis=0)
